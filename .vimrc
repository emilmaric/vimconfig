syntax on
filetype plugin indent on
colorscheme molokai


"""""""""""""""""""""""""
"PYTHON
"""""""""""""""""""""""""
"Tab Fix for Python
au FileType python set number
au FileType python set tabstop=4
au FileType python set shiftwidth=4
au FileType python set softtabstop=4
au FileType python set autoindent
au FileType python set textwidth=80
au FileType python set smarttab
au FileType python set expandtab

